
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="login" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" name="first_name">
        <p>Last name:</p>
        <input type="text" name="last_name">

        <p>Gender:</p>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" value="other">
        <label for="other">Other</label>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="Indonesian" selected>Indonesian</option>
            <option value="English">English</option>
            <option value="Malaysia">Malaysia</option>
        </select>

        <p>Languange Spoken:</p>
        <input type="checkbox" name="bahasa_indonesia" value="Bahasa Indonesia">
        <label for="bahasa-indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" name="other" value="Other">
        <label for="other">Other</label>

        <p>Bio:</p>
        <textarea name="biography" rows="10" cols="34"></textarea><br><br>
        <input type="submit" name="sign-up" id="sign-up">
    </form>
</body>
</html>