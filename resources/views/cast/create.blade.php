@extends('adminlte.master')

@section('title', 'Add New Cast')

@section('content')
<!-- form start -->
<form role="form" action="/cast" method="post">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}" placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="6">{{ old('bio', '') }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection
