@extends('adminlte.master')

@push('data-table-styles')
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('title', 'Cast Detail')

@section('content')
<a class="btn btn-success" href="/cast">Back</a>
<div class="card-body">
    <dl class="row">
        <dt class="col-sm-4">Nama</dt>
        <dd class="col-sm-8">{{ $cast->nama }}</dd>
        <dt class="col-sm-4">Umur</dt>
        <dd class="col-sm-8">{{ $cast->umur }}</dd>
        <dt class="col-sm-4">Bio</dt>
        <dd class="col-sm-8">{{ $cast->bio }}</dd>
    </dl>
</div>
        
@endsection
