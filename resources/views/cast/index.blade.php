@extends('adminlte.master')

@push('data-table-styles')
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('title', 'Casts Table')

@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<a class="btn btn-info" href="/cast/create">Add new cast</a>
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th style="width: 20px;">#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($cast as $key => $cast)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $cast->nama }}</td>
            <td>{{ $cast->umur }}</td>
            <td>{{ $cast->bio }}</td>
            <td>
                <a class="btn btn-info btn-sm mr-2" href="/cast/{{ $cast->id }}" style="float: left;">Show</a>
                <a class="btn btn-warning btn-sm mr-2" href="/cast/{{ $cast->id }}/edit" style="float: left;">Edit</a>
                <form action="/cast/{{ $cast->id }}" method="post" class="form-inline" style="float: left;">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="6" align="center">No casts</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection

@push('scripts')
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example2").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush
