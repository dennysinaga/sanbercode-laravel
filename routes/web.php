<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'AuthController@register');

Route::post('/login', 'HomeController@login');

Route::get('/back', 'AuthController@back');

Route::get('/master', function() {
    return view('adminlte.master');
});

Route::get('/index', function () {
    return view('items.index');
});

Route::get('/table', function () {
    return view('items.table');
});

Route::get('/data-tables', function() {
    return view('items.data-tables');
});

Route::prefix('cast')->group(function () {
    Route::get('', 'CastController@index');
    Route::get('/create', 'CastController@create');
    Route::post('', 'CastController@store');
    Route::get('/{cast_id}', 'CastController@show');
    Route::get('/{cast_id}/edit', 'CastController@edit');
    Route::put('/{cast_id}', 'CastController@update');
    Route::delete('/{cast_id}', 'CastController@destroy');
});
