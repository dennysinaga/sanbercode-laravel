<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDO;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function back(){
        return view('welcome');
    }
}
