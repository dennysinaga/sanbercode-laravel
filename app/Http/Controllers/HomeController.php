<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function login(Request $request){
        $firstName = $request->first_name;
        $lastName =  $request->last_name;
        $gender = $request->male;
        $gender = $request->female;
        $nationality = $request->nationality;
        $firstLang = $request->bahasa_indonesia;
        $secondLang = $request->english;
        $thirdLang = $request->other;
        $bioMain = $request->biography;

        return view('home',compact('firstName', 'lastName', 'gender', 'firstLang', 'secondLang','thirdLang', 'biography'));

    }
}
